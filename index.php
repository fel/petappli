<?php
/* 
 * La Pétappli se veut l'outil de gestion de base de données de la recyclerie
 * de Vallée Francaise.
 *
 * Copyright (C) 2024 Martin Chédaille <martin.ched@gmail.com>
 * Copyright (C) 2024 Félicien Pillot <felicien@informatiquelibre.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  */

require_once('controllers/vente.controller.php');
require_once('controllers/categorie.controller.php');
require_once('controllers/produit.controller.php');
require_once('controllers/lieu.controller.php');

$produitController   = new ProduitController();
$venteController     = new VenteController();
$categorieController = new CategorieController();

try{
    if( isset($_GET['page']) ) {

        switch ($_GET['page']) {
            case 'produits':
                $reponse = $produitController->getProduits();
		require ('views/produit.view.php');
		break;

            case 'ventes':
		list ($ventes, $tableau) =
		    $venteController->getProduitsVendus();
		require ('views/produitsVendus.view.php');
		break;

	    case 'formProduitCat':
		$reponse = $produitController->choixCategories();
		$urlSuivante = "formProduitSousCat";
		require ('views/formProduitCat.view.php');
		break;
		
	    case 'formProduitSousCat':
		$sousCats = $produitController->choixSousCategories($_POST['nom_categorie']);
		$urlSuivante = "formProduit";
		require ('views/formProduitSousCat.view.php');
		break;
		
	    case 'formProduit':
		list($lieux, $matieres) =
		    $produitController->formProduit($_POST['nom_categorie']);
		$urlSuivante = "addProduit";
		require('views/formProduit.view.php');
		require('views/formProduitFin.view.php');
		break;
		
	    case 'formVenteCat':
		$reponse = $produitController->choixCategories();
		$urlSuivante = "formVenteSousCat";
		require ('views/formProduitCat.view.php');
		break;

	    case 'formVenteSousCat':
		$sousCats = $produitController->choixSousCategories($_POST['nom_categorie']);
		$urlSuivante = "formVente";
		require('views/formProduitSousCat.view.php');
		break;

	    case 'formVente':
		list($sousCats, $lieux, $matieres) =
		    $produitController->formProduit($_POST['nom_categorie']);
		$urlSuivante = "addProduitVendu";
		if (isset($_POST["id_vente"])) {
		    $id_vente = $_POST["id_vente"];
		}
		require('views/formProduit.view.php');
		require('views/formProduitVente.view.php');
		break;

	    case 'addProduit':
		$_POST["photo"] = $_FILES['photo']['name'] ?? "pas de photo";
		var_dump ($_POST);
		if ($produitController->addProduit($_POST)) {
		    header ('location:index.php?page=produits');
		} else {
		    echo "Erreur dans l'ajout du produit";
		}
		break;
		
	    case 'deleteProduit':
		$produitController->deleteProduit($_POST['id_produit']);
		break;

	    case 'addProduitVendu':
		$id_produit = $_POST["id_produit"] ?? $produitController->addProduit($_POST);
		$prix_libre = $_POST["prix_libre"] ?? 0;
		$id_vente = $_POST["id_vente"] ?? $venteController->addVente($prix_libre);
		$venteController->addProduitVendu($id_produit, $id_vente);
		
		if ($_POST['action'] == "Terminer la vente") {
		    require('views/formVente.view.php');
		} elseif ($_POST['action'] == "Ajouter un autre produit") {
		    header ("location:index.php?page=formVenteCat&id_vente=$id_vente");
		}
		break;

	    case 'addVente':
		$prix_libre = $_POST['prix_libre'];
		$venteController->setVente($_POST["id_vente"], $_POST["prix_libre"]);
		header ('location:index.php?page=ventes');
		break;

	    case 'deleteVente':
		$venteController->deleteVente($_POST['id_vente']);
		break;

	    case 'gestionCategories':
		$tableau = $categorieController->listSousCategories();
		require ('views/gestionCat.view.php');
		break;

	    case 'addCategorie':
		$nom_categorie = $_POST['nom_categorie'];
		$sous_categorie = $_POST['sous_categorie'];
		$categorieController->formCategorie($nom_categorie, $sous_categorie);
		$tableau = $categorieController->listSousCategories();
		require ('views/gestionCat.view.php');
		break;

	    case 'deleteCategorie':
		$categorieController->deleteCategorie(
		    $_GET['nom_categorie']);
		break;

	    case 'deleteSousCategorie':
		$categorieController->deleteSousCategorie(
		    $_GET['nom_sous_categorie']);
		break;

	    case 'formCategorie':
		$tableau = $categorieController->tableauCategories();
		foreach ($tableau as $row)
		{
		    $categories[] = $row['nom_categorie'];
		}
		require('views/formCategorie.view.php');
		break;
		
	    default:
		throw new Exception ("message: Cette page n'existe pas!");
		break;
	}

    }else{
	header ('location:index.php?page=produits');
    } 

}catch(Exception $e){
    $error = $e->getMessage();
    require('views/error.view.php');      
}
