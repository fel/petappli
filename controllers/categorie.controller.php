<?php
/* 
 * La Pétappli se veut l'outil de gestion de base de données de la recyclerie
 * de Vallée Francaise.
 *
 * Copyright (C) 2024 Martin Chédaille <martin.ched@gmail.com>
 * Copyright (C) 2024 Félicien Pillot <felicien@informatiquelibre.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  */

require_once ('model/CategorieManager.php');

class CategorieController{
    protected $categorieManager;

    public function __construct() {
	$this->categorieManager = new CategorieManager();
    }
    
    public function tableauCategories(){
        $reponse = $this->categorieManager->getCategories();
	return $reponse;
    }

    public function listCategories(){
        $reponse = $this->categorieManager->getCategories();
        require ('views/categorie.view.php');
    }

    public function getCategories(){
        return $this->categorieManager->getCategories();
    }

    public function getSousCategories($nom_categorie){
	return $this->categorieManager->getSousCategories($nom_categorie);
    }

    public function listSousCategories(){
        $reponse = $this->categorieManager->getCategories();
        $tableau = array();
        while($categorie = $reponse->fetch()){
	    // Pour chaque catégorie, obtenez ses sous-catégories
	    $sousCategories = $this->categorieManager->getSousCategories($categorie["nom_categorie"]);
	    // Assignez les sous-catégories à la catégorie correspondante dans le tableau $reponse
	    $tableau[$categorie['nom_categorie']] = $sousCategories;
        }
	return $tableau;
    }

    public function formCategorie($nom_categorie, $sous_categorie){
        $this->categorieManager->formCategorie($nom_categorie, $sous_categorie, 0);
    }

    public function deleteCategorie($nom_categorie){ 
        $this->categorieManager->deleteCategorie($nom_categorie);
	header ('location:index.php?page=gestionCategories');
        exit();
    }

    public function deleteSousCategorie($nom_sous_categorie){ 
	$this->categorieManager->deleteSousCategorie($nom_sous_categorie);
	header ('location:index.php?page=gestionCategories');
	exit();
    }
}
