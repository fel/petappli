<?php
/* 
 * La Pétappli se veut l'outil de gestion de base de données de la recyclerie
 * de Vallée Francaise.
 *
 * Copyright (C) 2024 Martin Chédaille <martin.ched@gmail.com>
 * Copyright (C) 2024 Félicien Pillot <felicien@informatiquelibre.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  */

require_once('model/VenteManager.php');
require_once('model/ProduitManager.php');

class VenteController{
    protected $venteManager;
    protected $produitManager;

    public function __construct() {
	$this->venteManager = new VenteManager();
	$this->produitManager = new ProduitManager();
    }

    public function getVentes(){
        $reponse = $this->venteManager->getVentes();
	return $reponse;
    }

    public function addVente($prix_libre){
	return $this->venteManager->addVente($prix_libre);
    }

    public function addProduitVendu($id_produit, $id_vente){
	$this->venteManager->addProduitVendu ($id_produit, $id_vente);
    }

    public function getProduitsVendus(){
	# Initialisations
	$tableau = array();
	# On récupère les ventes
	$reponseVentes = $this->venteManager->getVentes();
	$ventes = $reponseVentes->fetchAll ();
	# Pour chaque vente
	foreach ($ventes as $col => $val) {
	    # On récupère les produits vendus associés
	    $reponsePV = $this->venteManager->getProduitsVendus($val["id_vente"]);
	    # Pour chaque produit vendu associé
	    while ($produitVendu = $reponsePV->fetch ()) {
		# On récupère les détails du produit
		$reponseP = $this->produitManager->getProduit($produitVendu["id_produit"]);
		$tableau[$val["id_vente"]][] = $reponseP->fetch ();
	    }
	}
	return array ($ventes, $tableau);
    }

    public function setVente($id_vente, $prix_libre){
	return $this->venteManager->setVente($id_vente, $prix_libre);	
    }

    public function deleteVente($id_vente){
	return $this->venteManager->deleteVente($id_vente);
    }    
}
