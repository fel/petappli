<?php
/* 
 * La Pétappli se veut l'outil de gestion de base de données de la recyclerie
 * de Vallée Francaise.
 *
 * Copyright (C) 2024 Martin Chédaille <martin.ched@gmail.com>
 * Copyright (C) 2024 Félicien Pillot <felicien@informatiquelibre.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  */

require_once('model/ProduitManager.php');
require_once('model/CategorieManager.php');
require_once('model/LieuManager.php');

class ProduitController{
    protected $produitManager;
    protected $categorieManager;
    protected $lieuManager;

    public function __construct() {
	$this->produitManager = new ProduitManager();
	$this->categorieManager = new CategorieManager();
	$this->lieuManager = new LieuManager();	
    }
    
    public function getProduits(){
        return $this->produitManager->getProduits();
    }

    public function getProduit($id_produit){
        return $this->produitManager->getProduit($id_produit);
    }

    public function choixCategories(){
        return $this->categorieManager->getCategories();
    }

    public function choixSousCategories($nom_categorie){
        return $this->categorieManager->getSousCategories($nom_categorie);
    }

    public function choixLieux () {
	return $this->lieuManager->getLieux ();
    }

    public function getMatieres() {
	return $this->produitManager->getMatieres();
    }

    public function formProduit($nom_categorie) {
	$lieux = $this->choixLieux();
	$matieres = $this->getMatieres();
	return array ($lieux, $matieres);
    }

    public function addProduit($formValues) {
	return $this->produitManager->addProduit(
	    $formValues['nom_sous_categorie'],
	    $formValues['nom_produit'],
	    $formValues['nom_matiere'],
	    $formValues['description'],
	    $formValues['lieu'],
	    $formValues['cout_reparation'],
	    $formValues['temps_passe'].":00",
	    $formValues['photo'],
	    $formValues['poids']);
    }

    public function deleteProduit($id_produit){
	return $this->produitManager->deleteProduit($id_produit);	
    }
}
