<?php
$title = "Ajout de catégorie";
ob_start()
?>

<h2>Ajouter une catégorie</h2>

<div class='formulaire'>
    <form onsubmit="FormSubmit(this)" id='formCategorie'
	  action="index.php?page=addCategorie"
	  method="POST">
	<?php
	$nom_categorie = $_GET["nom_categorie"] ?? "";
	$catfocus = $_GET["nom_categorie"] ? "" : "autofocus";
	$souscatfocus = $_GET["nom_categorie"] ? "autofocus" : "";
	?>
	<div>
            <label for="categorie">Categorie&nbsp;:&nbsp;</label>
	    <input class="champ" id="categorie" type="text" required <?=$catfocus?>
	           name="nom_categorie" value="<?=$nom_categorie?>"/>
	</div>
	<div>
	    <label for="sous_categorie">Sous-categorie&nbsp;:&nbsp;</label>
            <input class='champ' id="sous_categorie" type="string" required <?=$souscatfocus?>
		   name="sous_categorie"></input>
	</div>
	<input class='champ btn' id='form-categorieButton' type="submit" value="Envoyer le formulaire" />
    </form> 
</div>

<?php
$content = ob_get_clean();
require('base.view.php');
?>
