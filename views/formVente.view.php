<?php
$title = "Ajouter une vente";
ob_start();
?>

<form method="POST" action="index.php?page=addVente">
    <?php    
    foreach($_POST as $name => $value) {
    ?>
	<input type='hidden' name=<?=$name?> value=<?=$value?> />
    <?php
    }
    ?>

    <div class="champ" id="champPrix">
	<label for="prix_libre">Prix libre&nbsp;:&nbsp;</label>
	<input type="number" id="prix_libre" name="prix_libre"/>€
    </div>

    <div>
	<input class="btn" id='form-produitButton' type="submit"
	       value="Terminer la vente"/>
    </div>
</form> 

<?php
$content = ob_get_clean();
require('base.view.php');
?>
