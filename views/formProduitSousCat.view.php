<?php 
$title = "Ajouter un produit (2/3)";
ob_start();
?>

<h2>Ajouter un produit</h2>

<div class="formSousCat">
    <form id='form' action="index.php?page=<?=$urlSuivante ?>" method="POST">
	<input type="hidden" name="nom_categorie" value="<?=$_POST['nom_categorie']?>"/>
	<?php
	if (isset ($_GET['id_vente'])) {
	?>
	    <input type="hidden" name="id_vente" value="<?=$_GET['id_vente']?>"/>
	<?php
	}
	while($sousCategorie = $sousCats->fetch()) {
        ?>
            <button type="submit" name="nom_sous_categorie"
		    value="<?=$sousCategorie['nom_sous_categorie']?>">
		<?= $sousCategorie['nom_sous_categorie'] ?>
	    </button>
        <?php
        }
        ?>
    </form>
    <a href="/index.php?page=formCategorie&nom_categorie=<?=$_POST['nom_categorie']?>">
	<button>Autre...</button>
    </a>
</div>

<?php
$content = ob_get_clean ();
require('base.view.php');
?>
