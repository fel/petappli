<?php
$title = "Ajouter un produit (3/3)";
ob_start();
?>

<form action="index.php?page=<?=$urlSuivante ?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="nom_sous_categorie" value="<?=$_POST['nom_sous_categorie']?>"/>
    <div class="firstMargin">
	<label class="label" for="nom_produit">
	    Nom du produit&nbsp;:&nbsp;
	</label>
	<input class="champ" required autofocus
	       id='form-nom_produit' type="string"
	       name="nom_produit"/>
    </div>
    <div>
	<label for="poids">Poids (en g)&nbsp;:&nbsp;</label>
	<input class='champ' id='form-poids_categorie' type='number' required name="poids"/>
    </div>
    <div>
	<label class="label" for="matiere">Matière&nbsp;:&nbsp;</label>
	<select class="champ" id="matiere" onchange="DropDownChanged(this);" name="nom_matiere">
	    <?php
	    while ($matiere = $matieres->fetch ()) {
		echo "<option value='".$matiere["nom_matiere"]."'>".$matiere["nom_matiere"]."</option>";
	    }
	    ?>
	    <option value="">Autre...</option>
	</select>
    </div>

    <?php
    if ($urlSuivante == "addProduit") {
    ?>
	<div>
	    <textarea class="champ" maxlength=255
		      id="form-description" name="description"
		      placeholder="Description"></textarea>
	</div>
	<div>
	    <label class="label" for="photo">Ajouter une photo&nbsp;:</label>
	    <input type="file" accept="image/*"
		   id="photo" capture="user"
		   onchange="FileUpload(this)" name="photo" value="" />
	</div>
	<div>
	    <label for="lieu">Lieu de stockage&nbsp;:&nbsp;</label>
	    <input id="menu_lieu" type="hidden" name="lieu" />
	    <select id="lieu" class="champ" name="ddl_lieu" onchange="DropDownChanged(this);">
		<?php
		while ($lieu = $lieux->fetch ()) {
		    echo "<option value='".$lieu["lieu"]."'>".$lieu["lieu"]."</option>";
		}
		?>
		<option value="">Autre...</option>
	    </select>
	    <input type="text" name="lieu_txt" style="display: none;" />
	</div>
	<div>
	    <label class="label" for="cout_reparation">
		Coût de réparation&nbsp;:&nbsp;
	    </label>
	    <input class="champ cout" required
		   id='form-cout_reparation' type='number'
		   value="0" name="cout_reparation"/>&nbsp;€
	</div>
	<div>
	    <label class="label" for="temps_passe">Temps passé&nbsp;:&nbsp;</label>
	    <input class="champ temps" id="form-temps_passe"
		   name="temps_passe"
		   type="text" required
		   pattern="[0-9]{2}(:[0-9]{2})?"
		   value="00:00"
		   title="Écrire une durée au format hh ou hh:mm" onfocus="verifierDuree()">
	</div>
    <?php
    } else {
    ?>
	<input type="hidden" name="cout_reparation" value="0" />
	<input type="hidden" name="photo" value=" " />
	<input type="hidden" name="lieu" value=" " />
	<input type="hidden" name="temps_passe" value="00:00" />
    <?php
    }
    ?>
