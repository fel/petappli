<?php
/* 
 * La Pétappli se veut l'outil de gestion de base de données de la recyclerie
 * de Vallée Francaise.
 *
 * Copyright (C) 2024 Martin Chédaille, martin.ched@gmail.com
 * Copyright (C) 2024 Félicien Pillot <felicien@informatiquelibre.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  */

require("model/ProduitManager.php");
require ('controllers/categorie.controller.php');
require ('controllers/lieu.controller.php');

### TODO
### Ne pas afficher les produits vendus

$produitManager = new ProduitManager();
$reponse = $produitManager->getProduitsByFiltres($_GET["nom_categorie"]);
$tableau = array();

while ($ligne = $reponse->fetch ()) {
    # On récupère les détails du produit
    $tableau[] = $ligne;
}
$chainejson = json_encode($tableau);
$title = "Produits";

require ("base_vueGlobaleProduit.php");
?>
<content>
    <div class="tri">
	<div class="filtre">
	    <form action="" methode="GET">
		<select id="nom_categorie" class="champ" name="nom_categorie">
		    <option value="">Catégorie</option>
		    <?php
		    $filtreCat = new CategorieController();
		    $reponse = $filtreCat->tableauCategories();
		    while($categorie = $reponse->fetch()) {
		    ?>
			<option value="<?= $categorie['nom_categorie'] ?>">
			    <?= $categorie['nom_categorie'] ?></option>
		    <?php
		    } 
		    ?>     
		</select>

		<select id="nom_sous_categorie" class="champ" name="nom_sous_categorie">
		    <option value="">Sous-catégorie</option>
		    <?php
		    $filtreSousCat = new CategorieController();
		    $tableauSC = $filtreSousCat->listSousCategories();
		    foreach($tableauSC as $categories) {
			foreach($categories as $sousCategorie) {
		    ?>
			<option value="<?= $sousCategorie["nom_sous_categorie"] ?>">
			    <?= $sousCategorie['nom_sous_categorie'] ?></option>
		    <?php
		    }
		    }
		    ?>
		</select>

		<select id="lieu" class="champ" name="lieu">
		    <option value="">Lieu</option>
		    <?php
		    $filtreLieu = new LieuController();
		    $reponseL = $filtreLieu->listLieux();
		    while($lieu = $reponseL->fetch()) {
		    ?>
			<option value="<?= $lieu["lieu"] ?>">
			    <?= $lieu["lieu"] ?></option>			    
		    <?php
		    }
		    ?>
		</select>

		<span>Entre</span>
		<input type="date" name="dateE_debut" />
		<span>et</span>
		<input type="date" name="dateE_fin" />
		<input type="submit" value="Filtrer"/>
	    </form>
	</div>
    </div>
    <table>
	<thead>
	    <tr>
		<th><button onclick="filterBy('id_produit')">
		    ID
		</button></th>
		<th><button onclick="filterBy('nom_produit')">
		    Nom
		</button></th>
		<th><button onclick="filterBy('date_enregistrement')">
		    Date entrée
		</button></th>
		<th><button onclick="filterBy('nom_categorie')">
		    Catégorie
		</button></th>
		<th><button onclick="filterBy('nom_sous_categorie')">
		    Sous-catégorie
		</button></th>
		<th><button onclick="filterBy('poids')">
		    Poids
		</button></th>
		<th><button onclick="filterBy('cout_reparation')">
		    Coût
		</button></th>
		<th><button onclick="filterBy('temps_passe')">
		    Temps
		</button></th>
		<th><button onclick="filterBy('lieu')">
		    Lieu
		</button></th>
	    </tr>
	</thead>
	<tbody>
	</tbody>
    </table>
</content>
	</container>
    </body>
</html>
